<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login y Register - Felipe Settimini</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<body>

        <main>

            <div class="contenedor__todo">
                <div class="caja__trasera">
                    <div class="caja__trasera-login">
                        <h3>¿Ya registraste productos?</h3>
                        <p>Inicia sesión para entrar en la página</p>
                        <button id="btn__iniciar-sesion">Iniciar Sesión</button>
                    </div>
                    <div class="caja__trasera-register">
                        <h3>¿Aún no has registrado productos?</h3>
                        <p>Regístralos a continuación</p>
                        <button id="btn__registrarse">Regístrar</button>
                    </div>
                </div>

                <div class="contenedor__login-register">
                    <form action="" class="formulario__login">
                        <h2>Iniciar Sesión</h2>
                        <input type="text" placeholder="Correo Electronico">
                        <input type="password" placeholder="Contraseña">
                        <button>Entrar</button>
                    </form>

                    <form action="php/registro_ropa.php" method="POST" class="formulario__register">
                        <h2>Regístrar productos</h2>
                        <input type="text" placeholder="Marca del producto" name = "marcaproducto">
                        <input type="text" placeholder="Precio del producto" name = "precio">
                        <input type="text" placeholder="Color de ropa" name = "colorderopa">
                        <input type="text" placeholder="Tipo de prenda" name = "tipodeprenda">
                        <button>Regístrar</button>
                    </form>
                </div>
            </div>

        </main>

        <script src="assets/js/script.js"></script>
</body>
</html>